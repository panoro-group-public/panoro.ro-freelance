# Panoro.ro Client

The project is using:

-   **NextJS**
-   **Typescript**
-   **Tailwind**

## How to run

To start the the development server run:

```bash
npm run dev
```

## Style guide

The project is using Prettier for formatting the files.

The `includes` lines should be grouped in the following way:

```js
//= Functions & Modules
// Own
all the imports that are functions/classes/etc that are created by/for this project
// Others
all the imports that are functions/classes/etc from the packages/node_modules

//= Structures & Data
// Own
all the imports that are simple objects/enums that are created by/for this project
// Others
all the imports that are simple objects/enums from the packages/node_modules

//= React components
// Own
all the imports that are React components that are created by/for this project
// Others
all the imports that are React components from the packages/node_modules
```

## File structure

-   `docs` - the documentation directory
    -   `user_flow.svg` - a diagram showing the user flow
-   `public` - the public directory for the website
-   `src` - source files directory
    -   `components` - all React components (that are not pages)
        -   `[page_name]` - each specific components should be a directory named after the page name
    -   `pages` - all the pages of the website
        -   `api` - the server requests
    -   `server_utils` - all utilities related to the server/backend
    -   `client_utils` - all utilities related to the client/frontend
    -   `locale/ro` - the display texts for categories and transaction types

Rules for naming the files and directories:

-   directories should be in **snake_case**
-   React components should be in **PascalCase**
-   function files should be in **camelCase**

## Server utilities

-   `createOfferURL` - create the URL for an offer
-   `getHomeOffers` - get the offers that must be displayed on the home page
-   `getLocationsByIDs` - get locations by IDs
-   `searchLocationsByText` - search for locations by text

## API

-   `/api/offer_images/[id]` - returns the image for the cover of an offer

# Offers properties

-   If the `offer` object contains the property `characteristics.cuTVA` then the text `+ TVA` must be displayed after the price;
-   If the `offer` object contains the property `vrConfig` then the icon for `360 offer` must be displayed in the right corner of the offer;
-   Each `offer` has characteristics that must be displayed below the title of each offer. The characteristics are different depending on the `category` of the `offer`. The characteristics for each category can be find in `src/data/CategoriesCharacteristics.ts` (where the values of the array are the field/key names from the `characteristics` property of each `offer` object.
