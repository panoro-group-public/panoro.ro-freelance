//= Structures & Data
// Own
import Categories from 'src/data/Categories';
import Offer from '../data/Offer';

/**
 * @typedef {Object} HomeOfferType
 * @mixes Offer
 * @param {boolean} [vrConfig]
 *
 */
type HomeOffer = Offer & {
    vrConfig?: boolean;
};

/**
 * Get the offers for the home page from the server
 * @function getHomeOffers
 * @async
 *
 * @returns {Promise<HomeOffer[]>}
 * @exports module:server_utils/getHomeOffers
 */
export default async (): Promise<HomeOffer[]> => {
    return [
        {
            title: 'Apartament 3 camere zona Primaverii_Parcul Curcubeului',
            transactionType: 0,
            category: 'apartamente' as Categories,
            neighborhoodID: '5a6597ecd3fca42e90ca2fa8',
            cityID: '5a2274c6a50b2106947f2077',
            countyID: '5a2274bea50b2106947f2076',
            characteristics: {
                nrcamere: 3,
                bai: 1,
                etaj: 4,
                confort: 3,
                price: 72000,
                suprafataUtila: 87,
                anulConstructiei: 1988,
                tipulConstructiei: 1,
                tipCompartimentare: 0,
                parcare: 0,
                regimDeInaltime: 'P+4',
                cuTVA: true,
            },
            vrConfig: true,
            userID: '5e7c7b930f04c61fe854396c',
            code: 'KB8P14',
            classic_images_cover: '6124a7334aa16a18422b63f1',
        },
        {
            title: 'apartament 2 camere de vanzare ',
            transactionType: 0,
            category: 'apartamente' as Categories,
            neighborhoodID: '5a6597ecd3fca42e90ca2fa8',
            cityID: '5a2274c6a50b2106947f2077',
            countyID: '5a2274bea50b2106947f2076',
            characteristics: {
                nrcamere: 2,
                bai: 1,
                etaj: 8,
                confort: 0,
                suprafataUtila: 52,
                anulConstructiei: 1974,
                regimDeInaltime: 'P=  10',
                tipulConstructiei: 1,
                tipCompartimentare: 0,
                price: 50,
            },
            vrConfig: true,
            userID: '5f0c3c4562e3d66cc81c40b8',
            code: 'YU2I37',
            classic_images_cover: '61239dc84aa16a18422b63be',
        },
        {
            title: 'Apartament elegant cu 3 camere de inchiriat, zona Garii, Cluj-Napoca',
            transactionType: 1,
            category: 'apartamente' as Categories,
            neighborhoodID: '5a6597ecd3fca42e90ca2fa8',
            cityID: '5a2274c6a50b2106947f2077',
            countyID: '5a2274bea50b2106947f2076',
            characteristics: {
                nrcamere: 3,
                bai: 1,
                etaj: 8,
                confort: 3,
                price: 410,
                suprafataUtila: 65,
                anulConstructiei: 2015,
                regimDeInaltime: 'p+10',
                tipulConstructiei: 0,
                tipCompartimentare: 2,
                parcare: 2,
            },
            userID: '5f1144dc70aeee3f43321441',
            code: 'LU5S13',
            classic_images_cover: '611cf6ec4aa16a18422b6391',
            vrConfig: true,
        },
        {
            title: 'Casa de vânzare în Făget',
            transactionType: 0,
            category: 'case' as Categories,
            neighborhoodID: '5a6597ecd3fca42e90ca2fa8',
            cityID: '5a2274c6a50b2106947f2077',
            countyID: '5a2274bea50b2106947f2076',
            characteristics: {
                nrcamere: 3,
                bai: 3,
                confort: 0,
                etaj: 1,
                price: 194900,
                tipulConstructiei: 0,
                suprafataUtila: 150,
                suprafataTeren: 295,
                frontStradal: 13,
                anulConstructiei: 2000,
                parcare: 2,
                regimDeInaltime: 'S+P+M',
            },
            vrConfig: true,
            userID: '5ee88db16e84870ab82cc10c',
            code: 'CS7M4',
            classic_images_cover: '6087dc1d4aa16a18422b5d14',
        },
        {
            transactionType: 0,
            category: 'terenuri' as Categories,
            userID: '5e77d4b6134b6632abf86f45',
            title: 'Teren cu autorizatie pentru Casa familiala cu doua unitati locative',
            code: 'NFYO1',
            neighborhoodID: '5a6597ecd3fca42e90ca2fa8',
            cityID: '5a2274c6a50b2106947f2077',
            countyID: '5a2274bea50b2106947f2076',
            characteristics: {
                tipTeren: 2,
                nrcamere: 1,
                bai: 0,
                confort: 0,
                etaj: 1,
                clasificare: 0,
                suprafataTeren: 450,
                curent: true,
                apa: true,
                gaz: true,
                canalizare: true,
                price: 199000,
            },
            classic_images_cover: '600ac7224b33c01f14b9780e',
        },
        {
            title: 'Vanzare birou 40mp in Vision Marasti',
            transactionType: 0,
            category: 'birouri' as Categories,
            neighborhoodID: '5e4e8c9eeba22534aaa43f5b',
            cityID: '5a2274c6a50b2106947f2077',
            countyID: '5a2274bea50b2106947f2076',
            characteristics: {
                nrIncaperi: 1,
                nrLocuriParcare: 0,
                nrGrupuriSanitare: 1,
                nrcamere: 1,
                bai: 0,
                confort: 0,
                etaj: 1,
                price: 63500,
                suprafataUtila: 40,
                suprafataTeren: 0,
                anulConstructiei: 2014,
                regimDeInaltime: 'D+P+4E',
                tipImobilBirouri: 3,
                disponibilitateProprietate: 'Imediat',
                clasaBirouri: 1,
            },
            userID: '5e4e59c7eba22534aaa43f5a',
            code: 'IUGR71',
            vrConfig: true,
            classic_images_cover: '60be0fe74aa16a18422b5f67',
        },
    ];
};
