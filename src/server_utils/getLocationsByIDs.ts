//= Structures & Data
// Own
import Location from '../data/Location';

/**
 * Get the locations docs from the server by their IDs
 * @function getHomeOffers
 * @async
 *
 * @param {string[]} ids the IDs of the locations to search for
 *
 * @returns {Promise<Location[]>} the found locations as an array
 * @exports module:server_utils/getLocationByID
 */
export default async (ids: string[]): Promise<Location[]> => {
    let locations: Location[] = [];

    if (ids.includes('5a2274bea50b2106947f2076')) {
        locations.push({
            _id: '5a2274bea50b2106947f2076',
            name: 'Cluj',
            parentID: '5a2274b6a50b2106947f2075',
            level: 1,
            urlName: 'Cluj',
        });
    }

    if (ids.includes('5a2274c6a50b2106947f2077')) {
        locations.push({
            _id: '5a2274c6a50b2106947f2077',
            name: 'Cluj-Napoca',
            parentID: '5a2274bea50b2106947f2076',
            level: 2,
            urlName: 'Cluj-Napoca',
        });
    }

    if (ids.includes('5a6597ecd3fca42e90ca2fa8')) {
        locations.push({
            _id: '5a6597ecd3fca42e90ca2fa8',
            name: 'Gara',
            parentID: '5a2274c6a50b2106947f2077',
            level: 3,
            urlName: 'Gara',
        });
    }

    if (ids.includes('5e4e8c9eeba22534aaa43f5b')) {
        locations.push({
            _id: '5e4e8c9eeba22534aaa43f5b',
            name: 'Gheorgheni',
            parentID: '5a2274c6a50b2106947f2077',
            level: 3,
            urlName: 'Gheorgheni',
        });
    }

    return locations;
};
