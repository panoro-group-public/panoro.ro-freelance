//= Structures & Data
// Own
import Location from '../data/Location';

/**
 * Search for locations by text
 * @function searchLocationsByText
 *
 * @param {string} text the text to look for locations
 *
 * @returns {Promise<Location[]>} the found locations as an array
 */
export default async (text: string): Promise<Location[]> => {
    let locations: Location[] = [];

    if (text.includes('g')) {
        locations.push({
            _id: '5a6597ecd3fca42e90ca2fa8',
            name: 'Gara',
            parentID: '5a2274c6a50b2106947f2077',
            level: 3,
            urlName: 'Gara',
        });
    }

    return locations;
};
