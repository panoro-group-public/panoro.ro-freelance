//= Structures & Data
// Own
import Offer from '../data/Offer';

/**
 * Create the URL of an offer
 * @function createOfferURL
 *
 * @param {Offer} offer the offer object
 *
 * @returns {string} the URL
 */
export default (offer: Offer): string => {
    return `/e/${offer.code}`;
};
