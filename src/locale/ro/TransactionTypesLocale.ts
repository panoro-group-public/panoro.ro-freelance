//= Structures & Data
// Own
import TransactionTypes from '../../data/TransactionTypes';

export default {
    [TransactionTypes.VANZARI]: 'vanzari',
    [TransactionTypes.INCHIRIERI]: 'inchirieri',
    [TransactionTypes.REGIM_HOTELIER]: 'regim hotelier',
};
