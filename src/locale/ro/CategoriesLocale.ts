//= Structures & Data
// Own
import Categories from '../../data/Categories';

export default {
    [Categories.APARTAMENTE]: 'apartamente',
    [Categories.CASE]: 'case',
    [Categories.BIROURI]: 'birouri',
    [Categories.TERENURI]: 'terenuri',
    [Categories.SPATII_COMERCIALE]: 'spatiiComerciale',
    [Categories.SPATII_INDUSTRIALE]: 'spatiiIndustriale',
};
