//= Structures & Data
// Own
import Categories from './Categories';

export default {
    [Categories.APARTAMENTE]: ['suprafataUtila', 'nrcamere', 'parcare', 'etaj', 'bai', 'anConstructie'],
    [Categories.CASE]: ['suprafataUtila', 'nrcamere', 'bai', 'anConstructie', 'suprafataTeren'],
    [Categories.BIROURI]: ['suprafataUtila', 'nrIncaperi', 'bai', 'clasaBirouri'],
    [Categories.SPATII_COMERCIALE]: ['suprafataUtila', 'nrIncaperi'],
    [Categories.SPATII_INDUSTRIALE]: ['suprafataUtila', 'inaltimeSpatiu', 'suprafataTeren'],
    [Categories.TERENURI]: ['suprafataTeren', 'frontStradal', 'curent', 'apa', 'gaz', 'canalizare'],
};
