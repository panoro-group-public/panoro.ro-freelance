/**
 * @name Categories
 * @enum {string}
 * @readonly
 * @exports module:data/Categories
 */
enum Categories {
    APARTAMENTE = 'apartamente',
    CASE = 'case',
    BIROURI = 'birouri',
    SPATII_COMERCIALE = 'spatiiComerciale',
    SPATII_INDUSTRIALE = 'spatiiIndustriale',
    TERENURI = 'terenuri',
}

export default Categories;
