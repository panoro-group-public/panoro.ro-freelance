/**
 * @name TransactionTypes
 * @enum {number}
 * @readonly
 * @exports module:data/TransactionTypes
 */
enum TransactionTypes {
    VANZARI = 0,
    INCHIRIERI = 1,
    REGIM_HOTELIER = 2,
}

export default TransactionTypes;
