//= Structures & Data
// Own
import LocationLevels from './LocationLevels';

type Location = {
    _id: string;
    level: LocationLevels;
    name: string;
    parentID: string;
    urlName: string;
};

export default Location;
