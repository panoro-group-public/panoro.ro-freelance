//= Structures & Data
// Own
import TransactionTypes from './TransactionTypes';
import Categories from './Categories';

/**
 * @typedef {Object} Offer
 * @mixin
 * @property {string} code
 * @property {string} title
 * @property {Categories} category
 * @property {TransactionTypes} transactionType
 * @property {string} userID
 * @property {string} cityID
 * @property {string} [neighborhoodID]
 * @property {Object.<string, any>} characteristics
 * @property {number} characteristics.price
 */
interface Offer {
    code: string;
    title: string;
    category: Categories;
    transactionType: TransactionTypes;
    userID: string;
    cityID: string;
    countyID: string;
    neighborhoodID?: string;
    classic_images_cover: string;
    vrConfig?: any;
    characteristics: {
        price: number;
        cuTVA?: boolean;
        [key: string]: any;
    };
}

export default Offer;
