enum LocationLevels {
    COUNTY = 1,
    CITY = 2,
    NEIGHBORHOOD = 3,
}

export default LocationLevels;
