//= Functions & Modules
// Others
import path from 'path';
import fs from 'fs';

export default function getImageByIDRequest(req, res) {
    const { id } = req.query;

    const filePath = path.join(__dirname, '..', '..', '..', '..', '..', 'public', 'dummy_cover.jfif');
    const imageBuffer = fs.readFileSync(filePath);

    res.setHeader('Content-Type', 'image/jpg');
    res.send(imageBuffer);
}
